package com.dropchop.textonic.rest;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 17. 12. 21.
 */
public @interface RemoveMe {
}
