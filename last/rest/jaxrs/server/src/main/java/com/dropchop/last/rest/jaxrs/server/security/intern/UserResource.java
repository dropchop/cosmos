package com.dropchop.last.rest.jaxrs.server.security.intern;

import com.dropchop.last.model.api.rest.Constants.Paths;
import com.dropchop.last.model.api.security.Constants;
import com.dropchop.last.model.api.security.Constants.Domains;
import com.dropchop.last.model.dto.invoke.UserParams;
import com.dropchop.last.model.dto.rest.Result;
import com.dropchop.last.model.dto.security.User;
import com.dropchop.last.service.api.CommonExecContext;
import com.dropchop.last.service.api.security.SecurityService;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;
import java.util.List;
import java.util.UUID;

import static com.dropchop.last.model.api.security.Constants.PERM_DELIM;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 20. 01. 22.
 */
@RequestScoped
@Path(Paths.INTERNAL + Paths.Security.USER)
@RequiresPermissions(Domains.Security.USER + PERM_DELIM + Constants.Actions.VIEW)
public class UserResource implements
  com.dropchop.last.rest.jaxrs.api.intern.security.UserResource {

  @Inject
  @SuppressWarnings("CdiInjectionPointsInspection")
  SecurityService securityService;

  @Inject
  @SuppressWarnings("CdiInjectionPointsInspection")
  CommonExecContext<UserParams, User<?>> ctx;

  @Override
  public Result<User<?>> get() {
    return securityService.getUsers();
  }

  @Override
  public Result<User<?>> search(UserParams params) {
    return securityService.getUsers();
  }

  @Override
  public Result<User<?>> getByUuid(UUID id) {
    UserParams params = ctx.getParams();
    params.setIdentifiers(List.of(id.toString()));
    return securityService.getUsers();
  }
}
