package com.dropchop.last.model.api.test;

import com.dropchop.last.model.api.Model;
import com.dropchop.last.model.api.attr.Attribute;
import com.dropchop.last.model.api.localization.TitleTranslation;
import com.dropchop.last.model.api.marker.HasAttributes;
import com.dropchop.last.model.api.marker.HasCode;
import com.dropchop.last.model.api.marker.HasTitleTranslation;
import com.dropchop.last.model.api.marker.state.HasCreated;
import com.dropchop.last.model.api.marker.state.HasModified;

import java.util.SortedSet;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 9. 05. 22.
 */
public interface Node<T extends TitleTranslation, A extends Attribute<?>, N extends Node<T, A, N>>
  extends Model, HasCode, HasTitleTranslation<T>, HasAttributes<A>, HasCreated, HasModified {

  SortedSet<N> getChildren();
  void setChildren(SortedSet<N> children);
}
