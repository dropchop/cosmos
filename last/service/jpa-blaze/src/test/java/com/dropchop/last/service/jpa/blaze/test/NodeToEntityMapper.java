package com.dropchop.last.service.jpa.blaze.test;

import com.dropchop.last.model.api.attr.Attribute;
import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.test.Node;
import com.dropchop.last.model.entity.jpa.attr.EAttribute;
import com.dropchop.last.model.entity.jpa.test.ENode;
import com.dropchop.last.service.api.mapping.ToEntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 10. 03. 22.
 */
@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface NodeToEntityMapper extends ToEntityMapper<Node, CodeParams, ENode> {

  default Set<EAttribute<?>> toEntityAttributes(Set<Attribute<?>> value) {
    return new HashSet<>();
  }
}
