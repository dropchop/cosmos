package com.dropchop.last.service.jpa.blaze.test;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.test.Node;
import com.dropchop.last.model.entity.jpa.test.ENode;
import com.dropchop.last.service.api.mapping.ToDtoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 10. 03. 22.
 */
@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface NodeToDtoMapper extends ToDtoMapper<Node, CodeParams, ENode> {}
