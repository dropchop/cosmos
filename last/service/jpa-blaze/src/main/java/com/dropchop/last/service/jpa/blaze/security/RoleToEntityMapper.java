package com.dropchop.last.service.jpa.blaze.security;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.security.Role;
import com.dropchop.last.model.entity.jpa.security.ERole;
import com.dropchop.last.service.api.mapping.ToEntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 10. 03. 22.
 */
@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface RoleToEntityMapper extends ToEntityMapper<Role, CodeParams, ERole> {
}
