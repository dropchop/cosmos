package com.dropchop.last.service.jpa.blaze.security;

import com.dropchop.last.model.dto.rest.Result;
import com.dropchop.last.model.dto.security.*;
import com.dropchop.last.service.api.Implementation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import static com.dropchop.last.model.api.marker.Constants.Implementation.JPA_DEFAULT;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 7. 01. 22.
 */
@ApplicationScoped
public class SecurityService implements com.dropchop.last.service.api.security.SecurityService {

  @Inject
  @Implementation(JPA_DEFAULT)
  ActionService actionService;

  @Inject
  @Implementation(JPA_DEFAULT)
  DomainService domainService;

  @Override
  public LoginAccount getCurrentAccountInfo() {
    return null;
  }

  @Override
  public LoginAccount login() {
    return null;
  }

  @Override
  public Result<Action> getActions() {
    return actionService.search();
  }

  @Override
  public Result<Domain> getDomains() {
    return domainService.search();
  }

  @Override
  public Result<Role> getRoles() {
    return null;
  }

  @Override
  public Result<User<?>> getUsers() {
    return null;
  }
}
