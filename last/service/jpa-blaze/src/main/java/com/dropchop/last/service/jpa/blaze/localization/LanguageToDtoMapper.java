package com.dropchop.last.service.jpa.blaze.localization;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.localization.Language;
import com.dropchop.last.model.entity.jpa.localization.ELanguage;
import com.dropchop.last.service.api.mapping.ToDtoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 2. 02. 22.
 */
@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface LanguageToDtoMapper extends ToDtoMapper<Language, CodeParams, ELanguage> {
}
