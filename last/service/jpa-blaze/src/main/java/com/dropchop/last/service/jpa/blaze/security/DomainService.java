package com.dropchop.last.service.jpa.blaze.security;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.security.Domain;
import com.dropchop.last.model.entity.jpa.security.EDomain;
import com.dropchop.last.repo.jpa.blaze.security.DomainRepository;
import com.dropchop.last.service.api.CommonExecContext;
import com.dropchop.last.service.api.Implementation;
import com.dropchop.last.service.jpa.blaze.CrudServiceImpl;
import com.dropchop.last.service.jpa.blaze.ServiceConfiguration;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import static com.dropchop.last.model.api.marker.Constants.Implementation.JPA_DEFAULT;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 12. 01. 22.
 */
@Slf4j
@ApplicationScoped
@Implementation(JPA_DEFAULT)
public class DomainService extends CrudServiceImpl<Domain, CodeParams, EDomain, String>
  implements com.dropchop.last.service.api.security.DomainService {

  @Inject
  DomainRepository repository;

  @Inject
  DomainToDtoMapper toDtoMapper;

  @Inject
  DomainToEntityMapper toEntityMapper;

  @Override
  public ServiceConfiguration<Domain, CodeParams, EDomain, String> getConfiguration(CommonExecContext<CodeParams, Domain> execContext) {
    return new ServiceConfiguration<>(
      repository,
      toDtoMapper,
      toEntityMapper,
      execContext
    );
  }
}
