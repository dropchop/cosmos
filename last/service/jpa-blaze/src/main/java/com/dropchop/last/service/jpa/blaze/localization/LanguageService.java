package com.dropchop.last.service.jpa.blaze.localization;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.localization.Language;
import com.dropchop.last.model.entity.jpa.localization.ELanguage;
import com.dropchop.last.repo.jpa.blaze.localization.LanguageRepository;
import com.dropchop.last.service.api.CommonExecContext;
import com.dropchop.last.service.api.EntityByIdService;
import com.dropchop.last.service.api.Implementation;
import com.dropchop.last.service.jpa.blaze.CrudServiceImpl;
import com.dropchop.last.service.jpa.blaze.ServiceConfiguration;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import static com.dropchop.last.model.api.marker.Constants.Implementation.JPA_DEFAULT;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 12. 01. 22.
 */
@Slf4j
@ApplicationScoped
@Implementation(JPA_DEFAULT)
public class LanguageService extends CrudServiceImpl<Language, CodeParams, ELanguage, String>
  implements com.dropchop.last.service.api.localization.LanguageService, EntityByIdService<Language, ELanguage, String> {

  @Inject
  LanguageRepository repository;

  @Inject
  LanguageToDtoMapper toDtoMapper;

  @Inject
  LanguageToEntityMapper toEntityMapper;

  @Override
  public ServiceConfiguration<Language, CodeParams, ELanguage, String> getConfiguration(CommonExecContext<CodeParams, Language> ctx) {
    return new ServiceConfiguration<>(
      repository,
      toDtoMapper,
      toEntityMapper,
      ctx
    );
  }
}
