package com.dropchop.last.service.jpa.blaze.security;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.security.Domain;
import com.dropchop.last.model.entity.jpa.security.EDomain;
import com.dropchop.last.service.api.mapping.ToEntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 2. 02. 22.
 */
@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface DomainToEntityMapper extends ToEntityMapper<Domain, CodeParams, EDomain> {

}
