package com.dropchop.last.service.jpa.blaze.security;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.security.Role;
import com.dropchop.last.model.entity.jpa.security.ERole;
import com.dropchop.last.repo.jpa.blaze.security.RoleRepository;
import com.dropchop.last.service.api.CommonExecContext;
import com.dropchop.last.service.api.Implementation;
import com.dropchop.last.service.jpa.blaze.CrudServiceImpl;
import com.dropchop.last.service.jpa.blaze.ServiceConfiguration;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import static com.dropchop.last.model.api.marker.Constants.Implementation.JPA_DEFAULT;


/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 12. 01. 22.
 */
@Slf4j
@ApplicationScoped
@Implementation(JPA_DEFAULT)
public class RoleService extends CrudServiceImpl<Role, CodeParams, ERole, String>
  implements com.dropchop.last.service.api.security.RoleService {

  @Inject
  RoleRepository repository;

  @Inject
  RoleToDtoMapper toDtoMapper;

  @Inject
  RoleToEntityMapper toEntityMapper;

  @Override
  public ServiceConfiguration<Role, CodeParams, ERole, String> getConfiguration(CommonExecContext<CodeParams, Role> execContext) {
    return new ServiceConfiguration<>(
      repository,
      toDtoMapper,
      toEntityMapper,
      execContext
    );
  }
}
