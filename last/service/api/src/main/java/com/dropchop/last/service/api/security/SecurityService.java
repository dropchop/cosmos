package com.dropchop.last.service.api.security;

import com.dropchop.last.model.dto.rest.Result;
import com.dropchop.last.model.dto.security.*;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 7. 01. 22.
 */
public interface SecurityService {
  LoginAccount getCurrentAccountInfo();
  LoginAccount login();

  Result<Action> getActions();
  Result<Domain> getDomains();
  Result<Role> getRoles();
  Result<User<?>> getUsers();
}
