package com.dropchop.last.service.api.localization;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.localization.Language;
import com.dropchop.last.service.api.CrudService;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 20. 12. 21.
 */
public interface LanguageService extends CrudService<Language, CodeParams> {
}
