package com.dropchop.last.service.api;

import com.dropchop.last.model.api.Dto;
import com.dropchop.last.model.api.Entity;

import java.util.List;
import java.util.Optional;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 10. 03. 22.
 */
public interface EntityByIdService<D extends Dto, E extends Entity, ID> {
  Optional<E> findById(D dto);

  Optional<E> findById(ID id);

  List<E> findById(List<ID> ids);
}
