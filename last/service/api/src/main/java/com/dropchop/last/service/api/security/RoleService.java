package com.dropchop.last.service.api.security;

import com.dropchop.last.model.dto.invoke.CodeParams;
import com.dropchop.last.model.dto.security.Role;
import com.dropchop.last.service.api.CrudService;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 20. 12. 21.
 */
public interface RoleService extends CrudService<Role, CodeParams> {
}
