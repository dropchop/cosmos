package com.dropchop.last.service.api.mapping;

import com.dropchop.last.model.api.invoke.ExecContext.Listener;
import com.dropchop.last.model.api.invoke.Params;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 29. 04. 22.
 */
public interface MappingListener<P extends Params> extends Listener {
}
