package com.dropchop.last.service.api.mapping;

import com.dropchop.last.model.api.Dto;
import com.dropchop.last.model.api.Entity;
import com.dropchop.last.model.api.invoke.Params;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 29. 04. 22.
 */
public interface AfterToEntityListener<P extends Params>
  extends MappingListener<P> {
  void after(Dto dto, Entity entity, MappingContext<P> context);
}
