package com.dropchop.last.repo.jpa.blaze.security;

import com.dropchop.last.model.entity.jpa.security.EPermission;
import com.dropchop.last.repo.jpa.blaze.BlazeRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 19. 02. 22.
 */
@ApplicationScoped
public class PermissionRepository extends BlazeRepository<EPermission, UUID> {

  @Override
  protected Class<EPermission> getRootClass() {
    return EPermission.class;
  }
}
