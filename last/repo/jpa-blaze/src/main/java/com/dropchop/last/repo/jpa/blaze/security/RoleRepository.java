package com.dropchop.last.repo.jpa.blaze.security;

import com.dropchop.last.model.entity.jpa.security.ERole;
import com.dropchop.last.repo.jpa.blaze.BlazeRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 19. 02. 22.
 */
@ApplicationScoped
public class RoleRepository extends BlazeRepository<ERole, String> {

  @Override
  protected Class<ERole> getRootClass() {
    return ERole.class;
  }
}
