package com.dropchop.last.repo.jpa.blaze;

import com.dropchop.last.model.api.invoke.Params;
import com.dropchop.last.repo.api.ctx.CriteriaDecorator;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 3. 03. 22.
 */
public abstract class BlazeCriteriaDecorator<T, P extends Params> implements CriteriaDecorator<T> {

  private BlazeExecContext<T, P> context;

  public void init(BlazeExecContext<T, P> executionContext) {
    this.context = executionContext;
  }

  public BlazeExecContext<T, P> getContext() {
    return context;
  }
}
