package com.dropchop.last.repo.jpa.blaze.localization;

import com.dropchop.last.model.entity.jpa.localization.ELanguage;
import com.dropchop.last.repo.jpa.blaze.BlazeRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 19. 02. 22.
 */
@ApplicationScoped
public class LanguageRepository extends BlazeRepository<ELanguage, String> {

  @Override
  protected Class<ELanguage> getRootClass() {
    return ELanguage.class;
  }
}
