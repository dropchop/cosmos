package com.dropchop.last.repo.api.ctx;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 3. 03. 22.
 */
public interface CriteriaDecorator<E> {

  void decorate();
}
