package com.dropchop.last.model.api.tag;

import com.dropchop.last.model.api.Model;
import com.dropchop.last.model.api.marker.HasType;
import com.dropchop.last.model.api.marker.HasUuid;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 6. 01. 22.
 */
public interface Tag extends Model, HasUuid, HasType {
}
