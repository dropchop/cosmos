package com.dropchop.last.model.api.tag;

import com.dropchop.last.model.api.marker.HasName;
import com.dropchop.last.model.api.marker.HasUuidV3;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 6. 01. 22.
 */
public interface NamedTag extends Tag, HasName, HasUuidV3 {
}
