package com.dropchop.last.model.api.security;

import com.dropchop.last.model.api.Model;
import com.dropchop.last.model.api.localization.TitleTranslation;
import com.dropchop.last.model.api.marker.HasCode;
import com.dropchop.last.model.api.marker.HasTitleTranslation;

import java.util.SortedSet;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 11. 01. 22.
 */
public interface Domain<T extends TitleTranslation, A extends Action<T>>
  extends Model, HasCode, HasTitleTranslation<T> {

  SortedSet<A> getActions();
  void setActions(SortedSet<A> actions);
}
