package com.dropchop.last.model.api.localization;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 10. 01. 22.
 */
public interface TitleTranslation extends Translation {
  String getTitle();
  void setTitle(String title);
}
