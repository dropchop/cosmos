package com.dropchop.last.model.api;

import com.dropchop.last.model.api.marker.HasIdSameAsUuid;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 9. 01. 22.
 */
public interface ModelWithId extends Model, HasIdSameAsUuid {
}
