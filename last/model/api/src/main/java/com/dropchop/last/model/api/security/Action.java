package com.dropchop.last.model.api.security;

import com.dropchop.last.model.api.Model;
import com.dropchop.last.model.api.localization.TitleTranslation;
import com.dropchop.last.model.api.marker.HasCode;
import com.dropchop.last.model.api.marker.HasTitleTranslation;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 11. 01. 22.
 */
public interface Action<T extends TitleTranslation>
  extends Model, HasCode, HasTitleTranslation<T> {
}
