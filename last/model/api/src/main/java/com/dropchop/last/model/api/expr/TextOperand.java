package com.dropchop.last.model.api.expr;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 9. 12. 21.
 */
public interface TextOperand<T> extends Operand<T> {
}
