package com.dropchop.last.model.api.attr;

import com.dropchop.last.model.api.Model;
import com.dropchop.last.model.api.marker.HasName;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 17. 12. 21.
 */
public interface Attribute <T> extends Model, HasName {

  T getValue();
}
