package com.dropchop.last.model.api.expr.parse;

import com.dropchop.last.model.api.expr.BinaryOperator;
import com.dropchop.last.model.api.expr.Operator;
import com.dropchop.last.model.api.expr.UnaryOperator;
import com.dropchop.last.model.api.expr.math.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.dropchop.last.model.api.expr.ReservedSymbols.Math.*;

/**
 * Written just for the fun of it.
 *
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 16. 12. 21.
 */
@Slf4j
public class InfixMathExpressionParser extends InfixExpressionParser {

  private static final List<Class<?>> OPERATOR_PRECEDENCE = List.of(
    UnaryOperator.class,
    BinaryOperator.class
  );

  private static final Map<String, Operator> SUPPORTED_OPERATORS;

  static {
    Map<String, Operator> map = new LinkedHashMap<>();
    map.put(SINH_SYMBOL, new HyperbolicSine(SINH_SYMBOL));
    map.put(SIN_SYMBOL, new Sine(SIN_SYMBOL));
    map.put(POWER_SYMBOL, new Power(POWER_SYMBOL));
    map.put(MULTIPLICATION_SYMBOL, new Multiplication(MULTIPLICATION_SYMBOL));
    map.put(DIVSION_SYMBOL, new Division(DIVSION_SYMBOL));
    map.put(ADDITION_SYMBOL, new Addition(ADDITION_SYMBOL));
    map.put(SUBSTRACTION_SYMBOL, new Substraction(SUBSTRACTION_SYMBOL));

    SUPPORTED_OPERATORS = Collections.unmodifiableMap(map);
  }


  @Override
  protected Map<String, Operator> getSupportedOperators() {
    return SUPPORTED_OPERATORS;
  }

  @Override
  protected List<Class<?>> getOperatorPrecedence() {
    return OPERATOR_PRECEDENCE;
  }
}
