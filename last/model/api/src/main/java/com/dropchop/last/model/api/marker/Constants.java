package com.dropchop.last.model.api.marker;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 21. 04. 22.
 */
public interface Constants {
  interface Implementation {
    String JPA_DEFAULT = "jpa_default";
    String JPA_TEST    = "jpa_test";
  }
}
