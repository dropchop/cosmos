package com.dropchop.last.model.api.expr.parse;

import com.dropchop.last.model.api.expr.*;
import com.dropchop.last.model.api.expr.bool.And;
import com.dropchop.last.model.api.expr.bool.Not;
import com.dropchop.last.model.api.expr.bool.Or;
import com.dropchop.last.model.api.expr.bool.Xor;
import com.dropchop.last.model.api.expr.relational.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.dropchop.last.model.api.expr.ReservedSymbols.Bool.*;
import static com.dropchop.last.model.api.expr.ReservedSymbols.Relational.*;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 16. 12. 21.
 */
@Slf4j
public class InfixBoolExpressionParser extends InfixExpressionParser {

  private static final List<Class<?>> OPERATOR_PRECEDENCE = List.of(
    RelationalOperator.class,
    UnaryLeafOperator.class,
    BinaryLeafOperator.class,
    UnaryOperator.class,
    BinaryOperator.class
  );

  private static final Map<String, Operator> SUPPORTED_OPERATORS;

  static {
    Map<String, Operator> map = new LinkedHashMap<>();
    map.put(INDEPENDENT_WORD, new Near(INDEPENDENT_WORD));
    map.put(NEAR_WORD, new Near(NEAR_WORD));
    map.put(AND_WORD, new And(AND_WORD));
    map.put(XOR_WORD, new Xor(XOR_WORD));
    map.put(NOT_WORD, new Not(NOT_WORD));

    map.put(OR_WORD, new Or(OR_WORD));
    map.put(XOR_SYMBOL, new Xor(XOR_SYMBOL));
    map.put(OR_SYMBOL, new Or(OR_SYMBOL));
    map.put(AND_SYMBOL, new And(AND_SYMBOL));
    map.put(NEAR_SYMBOL, new Near(NEAR_SYMBOL));
    map.put(NOT_SYMBOL, new Not(NOT_SYMBOL));
    map.put(INDEPENDENT_SYMBOL, new Not(INDEPENDENT_SYMBOL));

    map.put(LTE_SYMBOL, new Lte(LTE_SYMBOL));
    map.put(GTE_SYMBOL, new Gte(GTE_SYMBOL));

    map.put(EQ_SYMBOL, new Eq(EQ_SYMBOL));
    map.put(EQ_SYMBOL_2, new Eq(EQ_SYMBOL_2));
    map.put(LT_SYMBOL, new Lt(LT_SYMBOL));
    map.put(GT_SYMBOL, new Gt(GT_SYMBOL));

    SUPPORTED_OPERATORS = Collections.unmodifiableMap(map);
  }


  @Override
  protected Map<String, Operator> getSupportedOperators() {
    return SUPPORTED_OPERATORS;
  }

  @Override
  protected List<Class<?>> getOperatorPrecedence() {
    return OPERATOR_PRECEDENCE;
  }
}
