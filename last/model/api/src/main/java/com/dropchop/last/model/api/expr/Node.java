package com.dropchop.last.model.api.expr;

import com.dropchop.last.model.api.Model;
import com.dropchop.last.model.api.attr.Attribute;
import com.dropchop.last.model.api.marker.HasAttributes;
import com.dropchop.last.model.api.marker.HasId;


/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 20. 11. 21.
 */
public interface Node extends Model, HasId, HasAttributes<Attribute<?>> {

  Position getPosition();
  void setPosition(Position position);

  String getValue();
  void setValue(String value);

  Node getParent();
  void setParent(Node node);

  BinaryTree getTree();
  void setTree(BinaryTree tree);
}
