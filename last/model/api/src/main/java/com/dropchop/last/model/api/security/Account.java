package com.dropchop.last.model.api.security;

import com.dropchop.last.model.api.Model;
import com.dropchop.last.model.api.marker.HasTitle;
import com.dropchop.last.model.api.marker.HasUuid;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 9. 01. 22.
 */
public interface Account extends Model, HasUuid, HasTitle {
}
