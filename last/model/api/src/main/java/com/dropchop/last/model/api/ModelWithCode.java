package com.dropchop.last.model.api;

import com.dropchop.last.model.api.marker.HasCode;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 9. 01. 22.
 */
public interface ModelWithCode extends Model, HasCode {
}
