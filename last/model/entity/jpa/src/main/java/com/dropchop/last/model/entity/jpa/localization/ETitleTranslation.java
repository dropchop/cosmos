package com.dropchop.last.model.entity.jpa.localization;

import com.dropchop.last.model.api.localization.TitleTranslation;
import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 17. 12. 21.
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@MappedSuperclass
@Embeddable
public class ETitleTranslation implements TitleTranslation {

  @NonNull
  @Column(name = "lang")
  private String lang;

  @OneToOne(targetEntity = ELanguage.class)
  @JoinColumn(name = "lang", referencedColumnName = "code", insertable = false, updatable = false)
  private ELanguage language;

  @NonNull
  @Column(name = "title")
  @EqualsAndHashCode.Exclude
  private String title;

  @Column(name="created")
  private ZonedDateTime created;

  @Column(name="modified")
  private ZonedDateTime modified;


  transient Boolean base;
}
