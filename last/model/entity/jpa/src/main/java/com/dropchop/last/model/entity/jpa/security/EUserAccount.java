package com.dropchop.last.model.entity.jpa.security;

import com.dropchop.last.model.api.security.UserAccount;
import com.dropchop.last.model.entity.jpa.EUuid;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 9. 01. 22.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class EUserAccount extends EUuid implements UserAccount {
  private String title;
}
