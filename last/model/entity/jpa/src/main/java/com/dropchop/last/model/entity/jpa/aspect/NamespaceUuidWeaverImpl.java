package com.dropchop.last.model.entity.jpa.aspect;

import com.dropchop.last.model.api.aspect.NamespaceUuidWeaver;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 7. 01. 22.
 */
@Aspect
public class NamespaceUuidWeaverImpl implements NamespaceUuidWeaver {

  @After(value = "set(String com.dropchop.last.model.entity..*name) " +
    "&& this(com.dropchop.last.model.api.marker.HasName) && this(com.dropchop.last.model.api.marker.HasUuidV3) " +
    "&& target(oModel) && args(name)",
    argNames = "oModel,name")
  public void changeClassWithName(Object oModel, String name) {
    NamespaceUuidWeaver.super.changeClassWithName(oModel, name);
  }

  @After(value = "set(String com.dropchop.last.model.entity..*code) " +
    "&& this(com.dropchop.last.model.api.marker.HasCode) && this(com.dropchop.last.model.api.marker.HasUuidV3) " +
    "&& target(oModel) && args(code)",
    argNames = "oModel,code")
  public void changeClassWithCode(Object oModel, String code) {
    NamespaceUuidWeaver.super.changeClassWithCode(oModel, code);
  }
}
