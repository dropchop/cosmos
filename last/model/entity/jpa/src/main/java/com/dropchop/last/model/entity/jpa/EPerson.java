package com.dropchop.last.model.entity.jpa;

import com.dropchop.last.model.api.Person;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 9. 01. 22.
 */
@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class EPerson extends EUuid implements Person {

  @Column(name="first_name")
  private String firstName;

  @Column(name="last_name")
  private String lastName;

  @Column(name="default_email")
  private String defaultEmail;

  @Column(name="default_phone")
  private String defaultPhone;

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(super.toString());
    sb.append("first='").append(firstName).append('\'');
    sb.append(", last='").append(lastName).append('\'');
    return sb.toString();
  }
}
