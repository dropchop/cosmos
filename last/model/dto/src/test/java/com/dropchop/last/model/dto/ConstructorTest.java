package com.dropchop.last.model.dto;

import com.dropchop.last.model.dto.localization.Country;
import com.dropchop.last.model.dto.localization.Language;
import com.dropchop.last.model.dto.invoke.IdentifierParams;
import com.dropchop.last.model.dto.tag.LanguageGroup;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 17. 12. 21.
 */
class ConstructorTest {

  @Test
  @SuppressWarnings("unused")
  public void constructor() {
    new Language("sl", Language.Script.Latn);
    new Language("sr", Language.Script.Cyrl, new Country("ME"));
    new Language();
    new Country();
    new Country("SI");
    new LanguageGroup();

    IdentifierParams params = new IdentifierParams();
    params = IdentifierParams.builder().identifier("a").identifier("b").build();
    assertEquals(List.of("a", "b"), params.getIdentifiers());
  }
}