package com.dropchop.last.model.dto.rest;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 18. 12. 21.
 */
public enum ResultCode {
  success,
  warning,
  error
}
