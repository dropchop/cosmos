package com.dropchop.last.model.dto.invoke;

import com.dropchop.last.model.api.attr.Attribute;
import com.dropchop.last.model.api.Dto;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.*;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 12. 01. 22.
 */
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class Params implements Dto, com.dropchop.last.model.api.invoke.Params {
  private String requestId;

  private String lang;

  @Builder.Default
  private String translationLang = null;

  @NonNull
  @Builder.Default
  private List<String> contentIncludes = new ArrayList<>();

  @NonNull
  @Builder.Default
  private List<String> contentExcludes = new ArrayList<>();

  @Builder.Default
  private Integer contentTreeLevel = null;

  @Builder.Default
  private String contentDetailLevel = null;

  @Builder.Default
  private String version = null;

  @Builder.Default
  private int size = 100;

  @Builder.Default
  private int from = 0;

  @Singular
  private List<String> states = new ArrayList<>();

  @Singular("sort")
  private List<String> sort = new ArrayList<>();

  @Singular
  private Set<Attribute<?>> attributes = new LinkedHashSet<>();
}
