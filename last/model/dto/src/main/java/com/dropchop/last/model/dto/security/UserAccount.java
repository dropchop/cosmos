package com.dropchop.last.model.dto.security;

import com.dropchop.last.model.dto.DtoId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikola Ivačič <nikola.ivacic@dropchop.org> on 9. 01. 22.
 */
@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class UserAccount extends DtoId implements com.dropchop.last.model.api.security.UserAccount {
  private String title;
}
